var gulp = require('gulp');
var nodemon = require('nodemon');

gulp.task('server', function () {
	nodemon({
		script: 'server.js',
		ext: 'js json',
		env: { 'NODE_ENV': 'development' },
		legacyWatch: true,
	});
});

gulp.task('default', ['server']);

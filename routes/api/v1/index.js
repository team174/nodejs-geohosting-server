var express = require('express');
var router = express.Router();

router.use('/feature', require('./feature'));
router.use('/user', require('./user'));

module.exports = router;
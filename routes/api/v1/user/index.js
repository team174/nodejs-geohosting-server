var express = require('express');
var router = express.Router();
var userModel = require('../../../../storage/model/user');
var verifyAuth = require('../../../../helpers/verifyAuth');

router.post('/', createUser);

router.post('/update', verifyAuth, updateUser);

router.post('/login', authUser);

router.post('/logout', outUser);

// middleware - verify auth
router.post('/password', verifyAuth, updatePassword);

function createUser(req, res, next) {
	var data = req.body;
	var UserModel = new userModel(data);
	UserModel.save(function (err, user) {
		if(err) return next(err);
		userModel.generateHash(user, function (err, user) {
			if(err) return next(err);
			res.status(201).json(user);
		});
	});
}

function updatePassword(req, res, next) {
	var data = req.body;
	userModel.findOne({'email': data.email}, function (err, user) {
		if (err) return next(err);
		user.password = data.password;
		userModel.generateHash(user, function (err, user) {
			if(err) return next(err);
			res.status(201).json(user);
		});
	});
}

function authUser(req, res, next) {
	var data = req.body;
	userModel.findOne({'email': data.email}, 'salt password', function (err, user) {
		if(err) return next(err);
		if(!user) return next('Not found');
		userModel.verifyHash(user, data.password, function (err) {
			if(!err) {
				res.status(201).json({auth: true});
				req.session.user_id = user.id;
				req.session.save(function(err) {});
			} else res.status(201).json({auth: false});
		});
	});
}

function outUser(req, res, next) {
	req.session.destroy(function() {});
	res.status(201).json({auth: false});
}

function updateUser(req, res, next) {
	var data = req.body;
	delete data.password;
	delete data.salt;
	delete data.banners;
	var user_id = req.session.user_id;
	userModel.update({_id: user_id}, data, function (err, user) {
		if(err) return next(err);
		if(!user) res.status(404).json({error: 'Не найдено'});
		else  res.status(201).json('success');
	});
}

module.exports = router;
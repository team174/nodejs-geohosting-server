var express = require('express');
var router = express.Router();

router.use('/within', require('./features-within'));
router.use('/near', require('./features-near'));
router.use('/', require('./features'));

module.exports = router;
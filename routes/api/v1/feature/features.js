var FeatureModel = require('../../../../storage/model/feature');
var UserModel = require('../../../../storage/model/user');
var BannerModel = require('../../../../storage/model/banner');
var verifyAuth = require('../../../../helpers/verifyAuth');

var express = require('express');
var router = express.Router();

router.post('/', verifyAuth, createFeature);
router.put('/:feature', verifyAuth, updateFeature);
router.delete('/:feature', verifyAuth, removeFeature);

router.route('/')
  .get(findFeatures);

router.route('/:feature')
  .get(getFeature);

/**
 * Express.Router param middleware.
 * Gets feature by Id.
 */
router.param('feature', function (req, res, next, id) {
  let Feature = FeatureModel;
  Feature.getById(id).exec()
    .then(function (feature) {
      if(!feature) {
        return next({
          status: 400,
          title: 'Bad Request',
          detail: 'Feature not found'
        });
      }
      req.feature = feature;
      next();
    }, next);
});

/**
 * Gets feature
 * @function
 * @name getFeature
 * @param {express.Request} req
 * @param {express.Response} res
 * @param {Function} next Express middleware callback
 */
function getFeature(req, res, next) {
  res.status(200).json(req.feature);
}

/**
 * Finds features
 * @function
 * @name findFeatures
 * @param {express.Request} req
 * @param {express.Response} res
 * @param {Function} next Express middleware callback
 */
function findFeatures(req, res, next) {
}

/**
 * Creates feauture
 * @function
 * @name createFeature
 * @param {express.Request} req
 * @param {express.Response} res
 * @param {Function} next Express middleware callback
 */
function createFeature(req, res, next) {
	var user_id = req.session.user_id;
  if(req.body.type == "FeatureCollection")
  {
    var features = req.body.features;
    let i;
    let promises = [];
    for (i = 0; i < features.length; ++i) {
			let Feature = FeatureModel;
			if(features[i].kind == 'Banner') Feature = BannerModel;
      promises.push(function(i){
        return new Promise(function(resolve) {
          let feature = new Feature(features[i]);
          feature.save(function (err, feature) {
            let data = {};
            data.data = feature;
            if(err) {
              data.error = 1;
            } else data.success = 1;

						UserModel.findOne({_id: user_id}, function (err, user){
							if(err) data.error = 1;
							else if(!user) data.error = 1;
							else {
								if(!user.banners) user.banners = [];
								user.banners = user.banners.concat({id: feature._id});
								user.save();
								data.success = 1;
							}
							resolve(data);
						});
          });
        });
      }(i));
    }
    Promise.all(promises)
      .then(function(results) {
        res.status(201).json(results);
      })
      .catch(function(err) {
        return next(err);
      });
    return;
  }
	let Feature = FeatureModel;
	if(req.body.kind == 'Banner') {
		Feature = BannerModel;
	}
  var feature = new Feature(req.body);

  feature.save(function (err, feature) {
    if(err) {
      return next(err);
    }
    if(!feature) return next("Not save");
		UserModel.findOne({_id: user_id}, function (err, user){
			if(err) next(err);
			else if(!user) next('Not Found');
			else {
				if(!user.banners) user.banners = [];
				user.banners = user.banners.concat({id: feature._id});
				user.save();
				res.status(201).json('created success');
			}
		});
  });
}

/**
 * Updates feature
 * @name updateFeature
 * @param {express.Request} req
 * @param {express.Response} res
 * @param {Function} next Express middleware callback
 */
function updateFeature(req, res, next) {
  var feature = req.feature;

  feature.set(req.body);
  feature.save(function (err) {
    if(err) {
      return next(err);
    }

    res.status(204).end();
  });
}

/**
 * Removes feature
 * @name removeFeature
 * @param {express.Request} req
 * @param {express.Response} res
 * @param {Function} next Express middleware callback
 */
function removeFeature(req, res, next) {
  var feature = req.feature;

  feature.remove(function (err) {
    if(err) {
      return next(err);
    }

    res.status(204).end();
  });
}

module.exports = router;
var mongoose = require("mongoose");
var base = require('./feature');
var model = base.discriminator('Banner', new mongoose.Schema({
	department: { type: String }
}), {discriminatorKey: 'kind'});

module.exports = model;

var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;
var connection = require('../index.js');
var schema = require('../schema/feature');
var GeoProjection = require('../../lib/projection');
var TileSystem = require('../../lib/tile-system');
var tileSystem = new TileSystem({
  projection: GeoProjection.create('wgs84Mercator')
});

var autoIncrement = require('mongoose-auto-increment');
autoIncrement.initialize(connection);
/**
 * Middleware that assigns quadKey on point coordinates.
 */
schema.pre('save', function (next) {
  var geometry = this.geometry;
  var coords = geometry.coordinates;

  if(geometry.type === 'Point') {
    this._quadKey = tileSystem.coordinatesToQuadKey(coords);
  }

  next();
});

schema.static('getById', function (id, cb) {
  var query;

  try {
    query = { _id: ObjectId.createFromHexString(id.toString()) };
  }
  catch (err) {
    query = { id: id };
  }

  return module.exports.findOne(query, cb);
});

schema.static('findInTiles', function (tiles, options, pQuery, cb) {
  var quadKeys = tiles.map(function (tile) {
    return tileSystem.tileNumberToQuadKey(tile, options.zoom);
  });
	let query;
	if(pQuery) query = { _quadKey: { $regex: '^(' + quadKeys.join('|') + ')' }, pQuery };
  else query = { _quadKey: { $regex: '^(' + quadKeys.join('|') + ')' } };

  if(options.clusterize) {
    // for version 2.6
    return clusterize.call(this, query, options, cb);
    // for version below than 2.6
    // return clusterizeMapReduce.call(this, query, options, cb);
  }

  return this.find(query, cb);
});

/**
 * Clusterize using MongoDB Aggregation pipeline (for version 2.6 and above).
 * @function
 * @name clusterize
 * @param {Object} query MongoDB query
 * @param {Object} options
 * @param {Function} cb Request callback
 * @returns {Promise}
 */
function clusterize(query, options, cb) {
  var gridSize = options.gridSize || 256;
  var zoom = options.zoom;
  var levels = zoom + 256 / gridSize;
  return this.aggregate({
    $match: query
  }, {
    $group: {
      _id: { $substr: [ '$_quadKey', 0, levels ] },
      // ids: { $push: '$_id' },
      //features: { $push: '$$ROOT' },
      feature: { $first: '$$ROOT' },
      lng: { $avg: { $arrayElemAt: ['$geometry.coordinates', 0] } },
      lat: { $avg: { $arrayElemAt: ['$geometry.coordinates', 1] } },
      north: { $max: { $arrayElemAt: ['$geometry.coordinates', 1] } },
      south: { $min: { $arrayElemAt: ['$geometry.coordinates', 1] } },
      east: { $max: { $arrayElemAt: ['$geometry.coordinates', 0] } },
      west: { $min: { $arrayElemAt: ['$geometry.coordinates', 0] } },
      count: { $sum: 1 }
    }
  }, function (err, data) {
    if(err) {
      return cb(err);
    }
    cb(null, data.map(function (it) {
      return it.count > 1? {
        id: it._id,
        type: 'Cluster',
        bbox: [ [ it.west, it.south ], [ it.east, it.north ] ],
        geometry: {
          type: 'Point',
          coordinates: [ it.lng, it.lat ]
        },
        number: it.count,
        properties: {
          // ids: it.ids,
          iconContent: it.count
        }
        // return Feature model instance.
      } : new module.exports(it.feature);
    }));
  });
}

/**
 * Clusterize using MongoDB MapReduce (for earlier versions than 2.6)
 * @function
 * @name clusterize
 * @param {Object} query MongoDB query
 * @param {Object} options
 * @param {Function} cb Request callback
 * @returns {Promise}
 */
function clusterizeMapReduce(query, options, cb) {
  var gridSize = options.gridSize || 256;
  var zoom = options.zoom;
  var levels = zoom + 256 / gridSize;

  return this.mapReduce({
    query: query,
    map: function () {
      emit(this._quadKey.substr(0, levels), this);
    },
    reduce: function (key, values) {
      var result = {
        ids: [],
        lat: 0,
        lng: 0,
        north: -90,
        south: 90,
        east: -180,
        west: 180 - 1e-10,
        count: values.length
      };

      values.forEach(function (it) {
        result.ids.push(it._id);
        result.lat += it.geometry.coordinates[1];
        result.lng += it.geometry.coordinates[0];
        result.north = Math.max(result.north, it.geometry.coordinates[1]);
        result.south = Math.min(result.south, it.geometry.coordinates[1]);
        result.east = Math.max(result.east, it.geometry.coordinates[0]);
        result.west = Math.min(result.west, it.geometry.coordinates[0]);
      });

      return result;
    },
    finalize: function (key, reduced) {
      return reduced.count? {
        id: key,
        type: 'Cluster',
        geometry: {
          type: 'Point',
          coordinates: [
            reduced.lng / reduced.count,
            reduced.lat / reduced.count
          ]
        },
        number: reduced.count,
        properties: {
          ids: reduced.ids,
          iconContent: reduced.count
        },
        bbox: [ [ reduced.west, reduced.south ], [ reduced.east, reduced.north ] ],
      } : reduced;
    },
    scope: {
      levels: levels
    }
  }, function (err, results) {
    if(err) {
      return cb(err);
    }

    cb(null, results.map(function (it) {
      return it.value;
    }));
  });
}

schema.plugin(autoIncrement.plugin, {model: 'Feature', field: 'id', startAt: 1});
module.exports = connection.model('Feature', schema);

var connection = require('../index.js');
var schema = require('../schema/user');
var hashKey = require('../../helpers/hashKey');

schema.static('generateHash', function (user, cb) {
	var salt = hashKey.genRandomString(16); /** Gives us salt of length 16 */
	var passwordData = hashKey.sha512(String(user.password), salt);
	user.password = passwordData.passwordHash;
	user.salt = passwordData.salt;
	module.exports.findByIdAndUpdate(user.id, { $set: { password: user.password, salt: user.salt }}, { new: true }, function (err, user) {
		if (err) cb(err, null);
		else cb(null, user);
	});
});

schema.static('verifyHash', function (user, password, cb) {
	var passwordHash = hashKey.sha512(String(password), user.salt);
	if(passwordHash.passwordHash === user.password){
		cb(null);
	} else cb(true);
});

module.exports = connection.model('User', schema);

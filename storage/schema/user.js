var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
		first_name: String,
		last_name: String,
		login: { type: String, unique: true, required: true },
		email: { type: String, unique: true },
		password: { type: String },
		salt: {type: String },
		banners: [{
			id: {
				type: mongoose.Schema.Types.ObjectId,
				ref: 'Banner'
			}
		}]
	},
	{ toJSON: {
		getters: true, transform: function (doc, ret, options) {
			ret.id = ret._id;
			delete ret._id;
			delete ret.__v;
			return ret;
		}
	}}
);

module.exports = userSchema;

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var GeometrySchema = require('./feature-geometry');

var schema = new Schema({
  type: { type: String, enum: ['Feature'], required: true },
  geometry: { type: GeometrySchema, required: true },
  properties: { type: Schema.Types.Mixed },
  _quadKey: { type: String }
  //options: { type: Schema.Types.Mixed } - без options пока, чтобы выставлять настройки на сервере
}, {
		toObject: { getters: true }, toJSON: {
			getters: true, transform: function (doc, ret, options) {
				ret.id = ret.id || ret._id;

				delete ret._id;
				delete ret.__v;
				delete ret._quadKey;

				return ret;
			}
		}, discriminatorKey: 'kind'
	});

module.exports = schema;

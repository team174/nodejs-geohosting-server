var userModel = require('../storage/model/user');

function verifyAuth(req, res, next) {
	if(req.session.user_id) {
		userModel.findById(req.session.user_id, function(err, user) {
			if(!(err || !user)) return next();
			else return next({auth: false});
		});
	} else return next({auth: false});
}

module.exports = verifyAuth;
